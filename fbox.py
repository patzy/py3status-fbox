"""
Freebox network monitoring module.
"""
import uuid
import hmac
from dataclasses import dataclass,asdict
import time
from datetime import datetime
import socket

import requests

# https://dev.freebox.fr/sdk/os/#
FREEBOX_API_VERSION_URL='http://mafreebox.freebox.fr/api_version'
APP_VERSION="0.1.0"
APP_NAME="Freebox Py3status"

@dataclass
class ApiVersion:
    box_model_name: str
    api_base_url: str
    https_port: int
    device_name: str
    https_available: bool
    box_model: str
    api_domain: str
    uid: str
    api_version: str
    device_type: str
    def url(self):
        return "https://{api_domain}:{https_port}{api_base_url}v4".format(
            api_domain=self.api_domain,
            https_port=self.https_port,
            api_base_url=self.api_base_url)

@dataclass
class ApiAuth:
    app_token: str
    track_id: int

@dataclass
class ApiTrack:
    status: str
    challenge: str
    password_salt: str
    def password(self,app_token):
        h = hmac.new(app_token.encode(),self.challenge.encode(),"sha1")
        pwd = h.hexdigest()
        return pwd

@dataclass
class ApiState:
    device_name: str
    app_id: str
    api_version: ApiVersion = None
    api_auth: ApiAuth = None
    api_track: ApiTrack = None
    def __post_init__(self):
        if isinstance(self.api_version,dict):
            self.api_version = ApiVersion(**self.api_version)
        if isinstance(self.api_auth,dict):
            self.api_auth = ApiAuth(**self.api_auth)
        if isinstance(self.api_track,dict):
            self.api_track = ApiTrack(**self.api_track)
    def dump(self):
        return asdict(self)

@dataclass
class ApiSession:
    session_token: str
    challenge: str
    password_salt: str
    permissions: dict
    password_set: bool

@dataclass
class NetStats:
    bw_up: int
    bw_down: int
    rate_up: int
    rate_down: int
    time: int

def rate_fmt(num, suffix="B"):
    for unit in ["", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi"]:
        if abs(num) < 1024.0:
            return f"{num:3.1f}{unit}{suffix}/s"
        num /= 1024.0
    return f"{num:.1f}Yi{suffix}/s"

class Py3status:

    refresh_delay = 60
    format = "freebox: ↑ {fmt_rate_up} ↓ {fmt_rate_down}"

    def __init__(self):
        self.api_state = None
        self.session = None
        self.stats = None
        self.api_url = None
        self.error = None

    def _error(self,msg):
        self.error = msg

    def _setup_api(self,api_state=None):
        if api_state is None:
            api_state = {"device_name": socket.gethostname(),
                         "app_id": uuid.uuid4().hex}
        api_state = ApiState(**api_state)
        print("Using configuration: %s"%api_state)
        if api_state.api_version is None:
            r = requests.get(FREEBOX_API_VERSION_URL)
            api_version = ApiVersion(**r.json())
            api_state.api_version = api_version
        return api_state

    def _request_auth(self,api_url,app_id,app_name,app_version,device_name):
        r = requests.post(api_url+'/login/authorize',verify=False,
                          json={"app_id": app_id,
                                "app_name": app_name,
                                "app_version": app_version,
                                "device_name": device_name})
        if r.status_code != 200:
            self._error("%d: %s"%(r.status_code,r.json()))
        return ApiAuth(**r.json()['result'])

    def _open_session(self,api_url,app_id,pwd):
        r = requests.post(api_url+'/login/session',verify=False,
                          json={"app_id": app_id,
                                "password": pwd})
        if r.status_code != 200:
            self._error("%d: %s"%(r.status_code,r.json()))
            return
        api_session = ApiSession(**r.json()['result'])
        return api_session

    def _close_session(self,api_url,api_session):
        r = requests.post(api_url+'/login/logout',verify=False,
                          headers={"X-Fbx-App-Auth": api_session.session_token})
        if r.status_code != 200:
            self._error("%d: %s"%(r.status_code,r.json()))
        self.session = None

    def _check_auth(self,api_url,api_state):
        api_auth = api_state.api_auth
        r = requests.get(api_url+'/login/authorize/%d'%api_auth.track_id,verify=False)
        if r.status_code != 200:
            self._error("%d: %s"%(r.status_code,r.json()))
            return
        print("Track:",r.json())
        api_track = ApiTrack(**r.json()['result'])
        print(api_track)
        if api_track.status == 'unknown':
            api_state.api_auth = None
            api_state.api_track = None
            self._error("Invalid app token.")
            return False
        if api_track.status == 'denied':
            api_state.api_auth = None
            api_state.api_track = None
            self._error("Authorization denied by user.")
            return False
        if api_track.status == 'timeout':
            api_state.api_auth = None
            api_state.api_track = None
            self._error("Timed out autorization.")
            return False
        if api_track.status == 'pending':
            self._error("Please authorize on your box !")
            api_state.api_track = None
            return False
        if api_track.status == 'granted':
            api_state.api_track = api_track
            self.message = None
            return True
        self.message = str(r.json())
        return False

    def _get_rrd(self,api_url,session,
                 db='net',fields=['rate_up','rate_down','bw_up','bw_down'],
                 since_seconds=50):
        date_start = time.time()-since_seconds
        r = requests.post(api_url+'/rrd',verify=False,
                          headers={"X-Fbx-App-Auth": session},
                          json={"db": db,"fields": fields,
                                "date_start": date_start})
        if r.status_code == 403:
            self.session = None
            return None
        if r.status_code != 200:
            self._error("Unable to retrieve data %s"%r.json()['result'])
            return None
        data = r.json()['result']['data'][0]
        return NetStats(**data)

    def post_config_hook(self):
        api_state = self.py3.storage_get('api_config')
        api_state = self._setup_api(api_state)
        self.api_state = api_state
        self.api_url = api_url = api_state.api_version.url()

    def _ensure_auth(self):
        api_state = self.api_state
        api_url = self.api_url
        if api_state.api_auth is None:
            print("Ensuring auth")
            api_auth = self._request_auth(api_url=api_url,app_id=api_state.app_id,app_name=APP_NAME,
                                          app_version=APP_VERSION,device_name=api_state.device_name)
            api_state.api_auth = api_auth

    def _ensure_session(self):
        api_url = self.api_url
        api_state = self.api_state
        if self._check_auth(api_url,api_state):
            print("Ensuring session")
            pwd = api_state.api_track.password(api_state.api_auth.app_token)
            session = self._open_session(api_url,api_state.app_id,pwd)
            if not session.permissions['settings']:
                self._error("Please authorize settings access in your box")
                self._close_session(api_url,session)
                return
            self.session = session

    def kill(self):
        if self.session is not None:
            self._close_session(self.api_url,self.session)
        self.py3.storage_set('api_config',self.api_state.dump())

    def fbox(self):
        try:
            if self.session is None:
                self._ensure_auth()
                self._ensure_session()
            if self.session is not None:
                net_stats = self._get_rrd(self.api_url,self.session.session_token)
        except Exception as err:
            self._error(str(err))
        if self.error is not None:
            self.output = self.error
        elif net_stats:
            data = {
                'rate_up': net_stats.rate_up,
                'rate_down': net_stats.rate_down,
                'fmt_rate_up': rate_fmt(net_stats.rate_up),
                'fmt_rate_down': rate_fmt(net_stats.rate_down),
                'bw_up': net_stats.bw_up,
                'bw_down': net_stats.bw_down,
                'fmt_bw_up': rate_fmt(net_stats.bw_up),
                'fmtbw_down': rate_fmt(net_stats.bw_down),
                'percent_up': float(net_stats.rate_up)/float(net_stats.bw_up)*100.0,
                'percent_down': float(net_stats.rate_down)/float(net_stats.bw_down)*100.0,
                'time': str(datetime.fromtimestamp(net_stats.time))
            }
            self.output = self.py3.safe_format(self.format,data)
        self.error = None
        return {
            'full_text': self.output,
            'cached_until': self.py3.time_in(self.refresh_delay)
        }

if __name__ == "__main__":
    """
    Run module in test mode.
    """
    from py3status.module_test import module_test
    module_test(Py3status)
